<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class ParticipantTest extends TestCase
{

    /**
     * A basic unit test example.
     *
     * @test
     */
    public function testAddParticipant()
    {
        $participant = factory(\App\Participant::class)->create()->getAttributes();

        $this->assertDatabaseHas('participants', [
            'email' => $participant['email'],
        ]);
        DB::table('participants')->where('email', $participant['email'])->delete();
    }
}
