<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class UserTest extends TestCase
{

    /**
     * A basic unit test example.
     *
     * @test
     */
    public function testUser()
    {
        $user = factory(\App\User::class)->create()->getAttributes();

        $this->assertDatabaseHas('users', [
            'email' => $user['email'],
        ]);
        DB::table('users')->where('email', $user['email'])->delete();
    }
}
