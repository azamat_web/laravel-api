<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('events')->insert([
            ['name' => 'День рождения', 'cities' => 'Москва', 'date' => '2020-07-07'],
            ['name' => 'Корпоратив', 'cities' => 'Барнаул', 'date' => '2020-08-22'],
            ['name' => 'Фуршет', 'cities' => 'Пермь', 'date' => '2020-09-03'],
            ['name' => 'День медиков', 'cities' => 'Саратов', 'date' => '2020-10-12'],
        ]);
    }
}
