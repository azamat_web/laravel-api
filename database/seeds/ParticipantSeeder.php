<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class ParticipantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('participants')->insert([
            ['firstname' => 'Nate', 'lastname' => 'Diaz', 'email' => 'rem@gmail.com', 'event_id' => 1],
            ['firstname' => 'Pablo', 'lastname' => 'Escobar', 'email' => 'rio@gmail.com', 'event_id' => 2],
            ['firstname' => 'El', 'lastname' => 'Chapo', 'email' => 'arho@gmail.com', 'event_id' => 1]
        ]);
    }
}
