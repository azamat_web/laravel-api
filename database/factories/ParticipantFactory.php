<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Participant;
use Faker\Generator as Faker;

$factory->define(Participant::class, function (Faker $faker) {
    return [
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'email' => $faker->email,
        'event_id' => 2,
        'updated_at' => '2020-07-12 08:47:56',
        'created_at' => '2020-07-12 08:47:56',

    ];
});
