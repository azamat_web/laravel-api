<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::prefix('/user')->group(function (){
    Route::post('login', 'api\v1\LoginController@login');
});

Route::middleware(['auth:api'])->group(function () {
    Route::get('events', 'api\v1\EventController@index');
    Route::get('participants/{id}', 'api\v1\ParticipantController@show');
    Route::get('participants', 'api\v1\ParticipantController@index');
    Route::post('participants', 'api\v1\ParticipantController@store');
    Route::put('participants/{id}', 'api\v1\ParticipantController@update');
    Route::delete('participants/{id}', 'api\v1\ParticipantController@destroy');
});
