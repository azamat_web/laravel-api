<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
    protected $fillable = [
        'firstname',
        'lastname',
        'email',
        'event_id',
    ];

    public function event()
    {
        return $this->belongsTo('App\Event');
    }

    public static function getEventData($event_id) {

        if ($event_id) {
            $participants = Participant::all()->where('event_id', $event_id);
        } else $participants = Participant::all();

        foreach($participants as $key => $participant ) {
            $participants[$key]['event']= $participant->event;
        }

        return $participants;
    }
}
