<?php

namespace App\Http\Controllers\api\v1;

use App\Event;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EventController extends Controller
{
    /**
     * @return mixed
     */


    public function index()
    {
        $event = DB::table('events')->get();
        return response()->json($event);
    }
}
