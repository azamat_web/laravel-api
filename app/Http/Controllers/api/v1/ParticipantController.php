<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Jobs\SendReminderEmail;
use App\Participant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ParticipantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     * @param Request $request
     */
    public function index(Request $request)
    {
        $participants = Participant::getEventData($request->input('event'));
        return response()->json($participants);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'event_id' => 'required|integer',
            'email' => 'email:rfc,dns|required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->getMessageBag());
        }

        try {
            $participant = new Participant();
            $participant->firstname = $request->firstname;
            $participant->lastname = $request->lastname;
            $participant->event_id = $request->event_id;
            $participant->email = $request->email;
            $participant->save();
            dispatch(new SendReminderEmail());
            return response()->json('is created success', 201);
        } catch (\Exception $e) {
            return response()->json($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $participant = Participant::find($id);
        return response()->json($participant);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'event_id' => 'required|integer',
            'email' => 'email:rfc,dns|required'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->getMessageBag(), 404);
        }

        try {
            $participant = Participant::find($request->id);
            $participant->firstname = $request->firstname;
            $participant->lastname = $request->lastname;
            $participant->event_id = $request->event_id;
            $participant->email = $request->email;
            $participant->save();

            return response()->json('is update success', 201);
        } catch (\Exception $e) {
            return response()->json($e->getMessage());
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $participant = Participant::find($id);
            if (!$participant)
                return response()->json('data not found', 404);

            if ($participant->delete())
                return response()->json('is deleted success', 200);

        } catch (\Exception $e) {
            return response()->json('error');
        }
    }
}
