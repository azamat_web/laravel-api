<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class LoginController extends Controller
{
    public function login(Request $request) {

        $login = $request->validate([
            'email' => 'required|string',
            'password' => 'required|string'
        ]);
        if (!Auth::attempt($login)) {
            return response(['message' => 'invalid login credential']);
        }
        $accessToken = Auth::user()->createToken('authToken')->accessToken;

        Storage::disk('local')->put('file.txt', 'Bearer ' . $accessToken);
        return response(['user' => Auth::user(), 'access_token' => $accessToken]);
    }
}
